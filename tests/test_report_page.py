from unittest import TestCase

from doe2_sim_parser.report.page import Page
from tests import sample


class PageTest(TestCase):
    def setUp(self):
        self.beps = sample / "sample - BEPS.sim"
        with self.beps.open(mode="r") as f:
            self.page = Page.from_file(f)

    def test_page(self):
        self.assertEqual(self.page.model_name, "sample")
        self.assertEqual(self.page.engine, "DOE-2.2-48z")
        self.assertEqual(self.page.date, "2/24/2019")
        self.assertEqual(self.page.time, "4:28:19")
        self.assertEqual(self.page.run_time, "1")

        self.assertEqual(self.page.code, "BEPS")
        self.assertEqual(self.page.name, "Building Energy Performance")
        self.assertEqual(self.page.weather, "CHICAGO, IL")

        with self.beps.open(mode="r") as f:
            self.assertSequenceEqual(self.page.page, f.readlines())
