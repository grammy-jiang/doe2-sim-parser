===============
DOE2-SIM-Parser
===============

Overview
========

.. image:: https://gitlab.com/grammy-jiang/doe2-sim-parser/badges/master/pipeline.svg?job=test:python3.7
   :target: https://gitlab.com/grammy-jiang/doe2-sim-parser/pipelines
   
.. image:: http://gitlab.com/grammy-jiang/doe2-sim-parser/badges/master/coverage.svg
   :target: https://grammy-jiang.gitlab.io/doe2-sim-parser


.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
   :alt: GNU General Public License v3.0

This project provides the DOE2 sim report splitting and parsing:

* split the sim report into pieces by the reports' names
* parse the sim report from pure text to csv file (comma-separated values),
  based on the requirement (configuration)
* upload the parsed sim reports to Google Spreadsheet

Requirements
============
   
* Python 3.6+
* Fully tested on Linux, but it should works on Windows, Mac OSX, BSD

Installation
============

The quick way:

   pip install doe2-sim-parser

For more details see the installation section in the documentation:
https://doe2-sim-parser.readthedocs.io/en/latest/installation.html

Documentation
=============

Documentation is available online at
https://doe2-sim-parser.readthedocs.io/en/latest/ and in the ``docs`` directory.

TODO
====

* [ ] Add Microsoft Office Excel creation for parsed sim reports
* [ ] Add Microsoft Office 365 support for parsed sim reports uploading
* [ ] Trigger Google Apps Script to do post-process after uploading the parsed
  sim reports

