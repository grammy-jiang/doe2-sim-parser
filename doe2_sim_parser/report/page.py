import re
from typing import IO, Tuple
from typing import Iterable


class Page(object):
    pattern_header = re.compile(
        r"""
        ^\x0c(?P<model_name>.+?)\s+
        (?P<engine>DOE-2\.\d+\-[\da-z]+)\s+
        (?P<date>[\/\d]+)\s+(?P<time>\d{1,2}:\d{1,2}:\d{1,2})\s+
        BDL\sRUN\s*(?P<run_time>\d+)$
        """,
        flags=re.VERBOSE,
    )
    pattern_title = re.compile(
        r"""
        ^REPORT-\s(?P<code>[A-Z\-]{4})\s(?P<name>.+?)\s+
        WEATHER\sFILE-\s(?P<weather>.+?)\s+$
        """,
        flags=re.VERBOSE,
    )

    def __init__(self, lines: Iterable[str]):
        self.page: Tuple[str] = tuple(lines)

        header = self.page[0]
        title = self.page[2]

        (
            self.model_name,
            self.engine,
            self.date,
            self.time,
            self.run_time,
        ) = self.pattern_header.match(header).groups()

        (self.code, self.name, self.weather) = self.pattern_title.match(title).groups()

    @classmethod
    def from_file(cls, file: IO):
        obj = cls(file.readlines())

        return obj
